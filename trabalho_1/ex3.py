"""
3. A conjectura de Goldbach é um dos mais antigos problemas por resolver em teoria de números.
Esta conjectura afirma que qualquer número inteiro 𝑝, que seja par e maior que 2, pode ser expresso como 𝑝=𝑞+𝑟
onde 𝑞 e 𝑟 são números primos. Escreva um programa que determine se a conjectura se verifica para um determinado
número inteiro e que mostre pares de inteiros primos que o comprovem.
"""

numerosPrimos = [2, 3, 5, 7, 11, 13, 17, 19] #amostra de números primos

def numeroPrimo(n): #definir se é um número primo ou não, retorna False ou True
    #números primos são maiores que 1
    if n > 1:
        for i in range(2,n):
            if (n % i) == 0:
                #print("{} * {} = {} logo, {} não é um número primo".format(i,n//i,n,n)) #usamos // para remover decimas e arrendondar o número
                return False
        else:
            #print(n,"é um número primo")
            return True
    else:
        #print(n,"não é maior que 1")
        return False

def conjenturaGoldbach(lista_primos, n):
    if ((n <= 2) or (n % 2 != 0)): #primeiro verificamos que o n é maior ou igual a 2 e par
        print("Insira um número maior que 2 e par")
    else:
        array = []
        resultado = []
        i = 0
        for numero_primo in lista_primos: #vamos iterar por cada número que exista na nossa lista de primos
            m = n - i #vamos ver que números são primos e menores que n
            if numeroPrimo(m) == True and i > 0: #se for primo vamos guardar num array
                array.append(m)
                if (len(array) > 1): #assim que tenhamos mais que um primo no array vamos iterar por eles
                    for k in range(len(array)):
                        if array[k] + array[k-1] == n: #até acharmos soma de 2 primos que sejam iguais a n
                            resultado.append(array[k])
                            resultado.append(array[k-1])
                        elif array[k] + array[k] == n:
                            resultado.append(array[k])
                            resultado.append(array[k])
                        else:
                            pass
            elif n <= 1:
                break
            i += 1
        print("{} = {} + {} ".format(n,resultado[0],resultado[1])) #resultado final

conjenturaGoldbach(numerosPrimos,14)
