"""
2. Escreva um programa que valide a seguinte afirmação: (𝑝∨𝑞)∧(¬𝑝∨𝑟)→(𝑞∨𝑟) é uma tautologia.
"""

#Queremos verificar que (𝑝∨𝑞)∧(¬𝑝∨𝑟)→(𝑞∨𝑟) é uma tautologia.

from ex1 import implicacao #importar o operador implicacao (função)

#Definição das variaveis
import random
p = random.choice([True, False])
q = random.choice([True, False])
r = random.choice([True, False])

#Como o operador implicação é uma função que apenas leva 2 argumentos, temos de prepara-los antes de passar para a função
p_para_implicacao = not(p) or r
q_para_implicacao = q or r

#Fazemos validação da expressão
#Se pVq e a conclusão é verdade, a expressão é verdadeira
if ((p or q) and (implicacao(p_para_implicacao,q_para_implicacao))):
    print('(𝑝∨𝑞)∧(¬𝑝∨𝑟)→(𝑞∨𝑟) é uma tautologia')
#Se a conclusão é verdade, a expressão é verdadeira
elif (implicacao(p_para_implicacao,q_para_implicacao) == True):
    print('(𝑝∨𝑞)∧(¬𝑝∨𝑟)→(𝑞∨𝑟) é uma tautologia')
#Se pVq é falso e a implicação também, a expressão é verdadeira
elif (((p or q) == False) and (implicacao(p_para_implicacao,q_para_implicacao) == False)):
    print('(𝑝∨𝑞)∧(¬𝑝∨𝑟)→(𝑞∨𝑟) é uma tautologia')
else:
    #Ver em que casos, se existentes, a expressão é falsa, ou seja, uma contigência ou contradição
    print('Sempre que P é {}, Q é {}, e R é {},'.format(p,q,r))
    print('é uma contigência ou contradição')
