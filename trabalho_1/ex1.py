"""
1. Defina uma função que implemente o operador lógico implicação e escreva um programa que verifique que 𝑝→𝑞 ≡ ¬𝑝∨𝑞.
"""
#Como não existe o operador implicação em Python, temos de cria-lo.
#Para tal utilizamos operadores que existem como ~(not) e V(or) e o nosso conhecimento de equivalências básica :)

#Criação do operador implicação, através de uma função
def implicacao(p,q):
    #Desde que P seja verdade, Q pode ser falso ou verdade
    if p == True:
        return q
    #Caso contrario, se o P for falso e o Q verdade, é verdade e se o P for falso e o Q falso é verdade, portanto...
    else:
        return True
    #Usando as equivalências basicas também podia ficar assim...
    # if not(p) or q:
    #     return True
    # else:
    #     return False

##Se corrermos o ficheiro ex1, caso não tenha esta validação, sempre que importarmos este ficheiro, corre tudo.
if __name__ == "__main__":
    #Atribuição de valores a P e Q
    #Importámos o modulo random para gerar valores boleano aleatorios para as variaveis
    import random
    p = random.choice([True, False])
    q = random.choice([True, False])

    #Validação se P -> Q é igual a ~P V Q
    if (implicacao(p,q) == (not(p) or q)):
        #Se for, dá a expressão
        print ("P->Q = ~P V Q")
        print ('P é ' + str(p) + ' | Q é ' + str(q) + '| ~P é ' + str(not (p)))
    else:
        #Senão diz que não é equivalente (o que implicaria que o programa estava mal..)
        print("Não é equivalente")
