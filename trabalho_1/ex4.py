"""
4. Escreva um programa que implemente o algoritmo Insertion Sort para ordenar por ordem crescente uma lista de números
inteiros distintos. Considere primeiro a lista [3,2,5,17,1] e depois uma lista de 300 números gerados aleatoriamente a
partir de um conjunto com 3000 números inteiros.
"""

import random

def insertionSort(alist):
   for index in range(1,len(alist)):

     currentvalue = alist[index]
     position = index

     while position>0 and alist[position-1]>currentvalue:
         alist[position]=alist[position-1]
         position = position-1

     alist[position]=currentvalue

alist = [3, 2, 5, 17, 1]
insertionSort(alist)
print("\n")
print("Lista de 3,2,5,17,1 ordenada - ",alist)

random_list = []
for i in range(300):
    random_list.append(random.randint(1,3000))

print("\n")
print("Lista de 3000 números aleatorios - ",random_list)
insertionSort(random_list)
print("\n")
print("Lista dos primeiros 300 números ordenada",random_list)
