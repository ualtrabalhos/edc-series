# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""


1.
#a) Defina uma função que implemente a cifra afim 𝑓(𝑛) = (19𝑛 − 7) mod 27 para
#encriptar mensagens escritas no alfabeto inglês (abcdefghijklmnopqrstuvwxyz) com 27
#letras, onde o espaço é a 27ª letra. A função deverá ter como argumento uma string
#contendo a mensagem a cifrar e deverá devolver uma string com a mensagem cifrada.
#Como exemplo codifique a mensagem ‘tempestade no mar’.


'''Dá a posição correta de cada caratere'''
s0 = 'abcdefghijklmnopqrstuvwxyz '
s=input('Texto a codificar: ')
##print('Texto a codificar:\n',s,'\n')
sn = []
for i in range (len(s)):
    for j in range (len(s0)):
        if s[i] == s0[j]:
            sn.append(j)
            break
        else:
            continue
print('Posição numérica original do texto a codificar:\n',sn, '\n')



'''Dá a posição desejada de cada caracter f(n) = (19n-7) mod27'''
sne=[]
for k in range (len(sn)): 
    sne.append((sn[k]*19-7) % 27)

se = ''
for l in range (len(sne)):
    for m in range (len(s0)):
        if sne[l] == m:
            se = se + s0[m]
            break
        else:
            continue
print('Texto codificado:\n',se,'\n')


'''Função Inversa'''

s0 = 'abcdefghijklmnopqrstuvwxyz '
s=se
print('Texto a descodificar:\n',s,'\n')
sn = []

for i in range (len(s)):
    for j in range (len(s0)):
        if s[i] == s0[j]:
            sn.append(j)
            break
        else:
            continue
print('Posição numérica original do texto a descodificar:\n',sn, '\n')

sne=[]
for k in range (len(sn)):
    sne.append((10*(sn[k]+7)) % 27)
print('Posição numérica descodificada:\n',sne,'\n')
se = ''
for l in range (len(sne)):
    for m in range (len(s0)):
        if sne[l] == m:
            se = se + s0[m]
            break
        else:
            continue
print('Texto descodificado:\n',se,'\n')