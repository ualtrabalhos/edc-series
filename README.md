# Serie de exercícios para Estruturas Discretas da Computação
**Realizado por: Aquila Monteiro, Fábio Albuquerque, Nuno Serrador** 
--------
# Trabalho #1 - EDC
## Enunciado do trabalho

1. Defina uma função que implemente o operador lógico implicação e escreva um programa que verifique que 𝑝→𝑞 ≡ ¬𝑝∨𝑞.  

2. Escreva um programa que valide a seguinte afirmação: (𝑝∨𝑞)∧(¬𝑝∨𝑟)→(𝑞∨𝑟) é uma tautologia.   

3. A conjectura de Goldbach é um dos mais antigos problemas por resolver em teoria de números. Esta conjectura afirma que qualquer número inteiro 𝑝, que seja par e maior que 2, pode ser expresso como 𝑝=𝑞+𝑟 onde 𝑞 e 𝑟 são números primos. Escreva um programa que determine se a conjectura se verifica para um determinado número inteiro e que mostre pares de inteiros primos que o comprovem.  

4. Escreva um programa que implemente o algoritmo Insertion Sort para ordenar por ordem crescente uma lista de números inteiros distintos. Considere primeiro a lista [3,2,5,17,1] e depois uma lista de 300 números gerados aleatoriamente a partir de um conjunto com 3000 números inteiros.

# Trabalho #2 - EDC
## Enunciado do trabalho

1. Defina uma função que implemente a cifra afim 𝑓(𝑛) = (19𝑛 − 7) mod 27 para
encriptar mensagens escritas no alfabeto inglês (abcdefghijklmnopqrstuvwxyz) com 27
letras, onde o espaço é a 27ª letra. A função deverá ter como argumento uma string
contendo a mensagem a cifrar e deverá devolver uma string com a mensagem cifrada.
Como exemplo codifique a mensagem ‘tempestade no mar’.

2. Defina a função inversa de 𝑓(𝑛) = (19𝑛 − 7) mod 27. Esta função deverá ter como
argumento uma string que contém a mensagem codificada pela cifra 𝑓(𝑛) e deverá
devolver uma string com a mensagem descodificada. Como exemplo descodifique a
seguinte mensagem ‘ypopbyublpttubxubpldtpnu’.

3. Defina uma função que calcule a função de Euler 𝜙. Como exemplos determine
𝜙(4), 𝜙(5), 𝜙(116), 𝜙(117), 𝜙(2018).

4. Dados dois números inteiros 𝑎 e 𝑛, primos entre si, determine o inverso de 𝑎 módulo
𝑛, ou seja, resolva em ℤ𝑛 a congruência 𝑎𝑥 ≡𝑛 1. Como exemplos resolva as seguintes
congruências 4𝑥 ≡9 1, 5𝑥 ≡27 1 e 99𝑥 ≡335 1.
