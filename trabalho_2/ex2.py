"""
3. Defina uma função que calcule a função de Euler 𝜙. Como exemplos determine
𝜙(4), 𝜙(5), 𝜙(116), 𝜙(117), 𝜙(2018).
"""

def numeroPrimo(n): #função do trabalho_1, ex3
    if n > 1:
        for i in range(2,n):
            if (n % i) == 0:
                return False
        else:
            return True
    else:
        return False

def euler(num): #funcao para determinar o numero de euler
    y = num
    for i in range(2,num+1):
        if numeroPrimo(i) and num % i == 0:
            y *= 1 - 1.0/i
    return int(y)

print(str(euler(4)) + "\n" + str(euler(5)) + "\n" + str(euler(116)) + "\n" + str(euler(117)) + "\n" + str(euler(2018)))
