"""
4. Dados dois números inteiros 𝑎 e 𝑛, primos entre si, determine o inverso de 𝑎 módulo
𝑛, ou seja, resolva em ℤ𝑛 a congruência 𝑎𝑥 ≡𝑛 1. Como exemplos resolva as seguintes
congruências 4𝑥 ≡9 1, 5𝑥 ≡27 1 e 99𝑥 ≡335 1.
"""

def congruencia(a,n):
	for x in range(1,n):
		if(a * x-1)%n ==0:
			print("O inverso de {} mod {} é {}".format(a,n,x))
			break
		else:
			continue

congruencia(4,9)
congruencia(5,27)
congruencia(99,335)
