"""
1. Defina uma função que implemente a cifra afim 𝑓(𝑛) = (19𝑛 − 7) mod 27 para
encriptar mensagens escritas no alfabeto inglês (abcdefghijklmnopqrstuvwxyz) com 27
letras, onde o espaço é a 27ª letra. A função deverá ter como argumento uma string
contendo a mensagem a cifrar e deverá devolver uma string com a mensagem cifrada.
Como exemplo codifique a mensagem ‘tempestade no mar’.
"""

s0 = 'abcdefghijklmnopqrstuvwxyz '
s = "tempestade no mar"

def codificarString(string):
    sn = []
    for i in range (len(s)):
        for j in range (len(s0)):
            if s[i] == s0[j]:
                sn.append(j)
                break
            else:
                continue

    sne=[]
    for k in range (len(sn)):
        sne.append((sn[k]*19-7) % 27)

    se = ''
    for l in range (len(sne)):
        for m in range (len(s0)):
            if sne[l] == m:
                se = se + s0[m]
                break
            else:
                continue
    return(se) #texto codificado

print("O texto {} codificado é {}".format(s,codificarString(s)))

"""
2. Defina a função inversa de 𝑓(𝑛) = (19𝑛 − 7) mod 27. Esta função deverá ter como
argumento uma string que contém a mensagem codificada pela cifra 𝑓(𝑛) e deverá
devolver uma string com a mensagem descodificada. Como exemplo descodifique a
seguinte mensagem ‘ypopbyublpttubxubpldtpnu’.
"""
texto = "ypopbyublpttubxubpldtpnu"

def descodificarTexto(string):
    s0 = 'abcdefghijklmnopqrstuvwxyz '
    s = string
    sn = []
    for i in range (len(s)):
        for j in range (len(s0)):
            if s[i] == s0[j]:
                sn.append(j)
                break
            else:
                continue

    sne=[]
    for k in range (len(sn)):
        sne.append((10*(sn[k]+7)) % 27)

    se = ''
    for l in range (len(sne)):
        for m in range (len(s0)):
            if sne[l] == m:
                se = se + s0[m]
                break
            else:
                continue
    return(se)

print("O texto ypopbyublpttubxubpldtpnu descodificado é", descodificarTexto(texto))
